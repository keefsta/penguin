#!/usr/bin/python
#--------------------------------------
#    ___  ___  _ ____          
#   / _ \/ _ \(_) __/__  __ __ 
#  / , _/ ___/ /\ \/ _ \/ // / 
# /_/|_/_/  /_/___/ .__/\_, /  
#                /_/   /___/   
#
#    Stepper Motor Test
#
# A simple script to control
# a stepper motor.
#
# Author : Matt Hawkins
# Date   : 11/07/2012
#
# http://www.raspberrypi-spy.co.uk/
#
#--------------------------------------
#!/usr/bin/env python

# Import required libraries
import time, urllib, json, threading, random
import RPi.GPIO as GPIO
from subprocess import call
from messages import getMessages

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define GPIO signals to use
# Pins 18,23,24,25
# GPIO24,GPIO25,GPIO8,GPIO7
StepPins = [18,23,24,25]
soundPin = 7

# Set all pins as output
# step pin
for pin in StepPins:
  print "Setup pins"
  GPIO.setup(pin,GPIO.OUT)
  GPIO.output(pin, False)
#sound pin
GPIO.setup(soundPin, GPIO.OUT)

# Define some settings
StepCounter = 0
twistCounter = 0
WaitTime = 0.0009

maxRotations=60
total=0
lastTotal=0

message=""
message=getMessages
toSay=[]
messages=["thank you {0} from {1}"]
# Define simple sequence
StepCount1 = 4
Seq1 = []
Seq1 = range(0, StepCount1)
Seq1[0] = [1,0,0,0]
Seq1[1] = [0,1,0,0]
Seq1[2] = [0,0,1,0]
Seq1[3] = [0,0,0,1]

# Define advanced sequence
# as shown in manufacturers datasheet
StepCount2 = 8
Seq2 = []
Seq2 = range(0, StepCount2)
Seq2[0] = [1,0,0,0]
Seq2[1] = [1,1,0,0]
Seq2[2] = [0,1,0,0]
Seq2[3] = [0,1,1,0]
Seq2[4] = [0,0,1,0]
Seq2[5] = [0,0,1,1]
Seq2[6] = [0,0,0,1]
Seq2[7] = [1,0,0,1]

# Choose a sequence to use
Seq = Seq2
StepCount = StepCount2
rotationCounter =0
r=range(0,4)


def checkFeed():
  while 1:
    global total, lastTotal
    global message
    global toSay
    #jsonURL=urllib.urlopen("https://support.wwf.org.uk/totaliser?api_user=wwf_totaliser&pid=10&api_key=B5Hp03Q6x3xZ8egn0J95lY1D9x4oEgZm")
    jsonURL=urllib.urlopen("http://counters.wwf.org.uk/jsoncachr/?pid=10")
    #jsonURL=open("json.js")
    feed=json.loads(jsonURL.read())
    #feed=json.load(jsonURL)
    total = int(feed["numberOfSupporters"]["today"])
#    message = "Good news everyone. A penguin has been adopted by " + feed["supporters"][0]["name"] 
    numberUnread = total - lastTotal
#    print "unread" +str(numberUnread)
    if numberUnread!=0:
      unread=feed["supporters"][-numberUnread:]
      for i in range (0, len(unread)):
        name=feed["supporters"][i]["name"]
        location=feed["supporters"][i]["location"]
        message = messages[random.randint(0,len(messages)-1)].format(name,location)
        toSay.append(message)

    print toSay
    print total
    jsonURL.close()
    time.sleep(120)

def getMessages():
  while 1:
    global messages
    jsonURL=urllib.urlopen("https://spreadsheets.google.com/feeds/cells/1Y2mhJmyhaAAu1SZKgn_-UCIf4Y8Os5FcSzdcCMssbU0/od6/public/values?alt=json")
    feed=json.loads(jsonURL.read())
    feedPart=feed["feed"]["entry"]
    messages=[]
    for i in range(0,len(feedPart)):
      messages.append(feed["feed"]["entry"][i]["gs$cell"]["$t"])
 #   print messages
    jsonURL.close()
    time.sleep(300)

#messages=getMessages()

#http://www.urbandictionary.com/define.php?term=bobbins
# Start checking adoption feed
checkFeed = threading.Thread(target=checkFeed)
checkFeed.daemon=True
checkFeed.start()

loadMessages = threading.Thread(target=getMessages)
loadMessages.daemon=True
loadMessages.start()

# Start main loop
print "starting loop"
while 1==1:
  
  for pin in r:
    xpin = StepPins[pin]
    if Seq[StepCounter][pin]!=0:
   #   print " Step %i Enable %i" %(StepCounter,xpin)
      GPIO.output(xpin, True)
    else:
      GPIO.output(xpin, False)
 
  StepCounter += 1

  # If we reach the end of the sequence
  # start again
  if (StepCounter==StepCount):
    StepCounter = 0
    rotationCounter+=1
  if (StepCounter<0):
    StepCounter = StepCount

  if rotationCounter > maxRotations:
    Seq.reverse()
    r.reverse()
    
    if total!=lastTotal:
#      print "play sound"
      for i in range(0,min(len(toSay),5)): 
        call(["./speech.sh",toSay[i]])
        time.sleep(0.25)
#        call(["espeak",toSay[i]])
        print toSay[i]
#      GPIO.output(soundPin, True)
#      time.sleep(0.15)
#      GPIO.output(soundPin, False)
      toSay=[]
      lastTotal=total
    rotationCounter=0
    
  # Wait before moving on
  time.sleep(WaitTime)

#end of main looop
  
  
